<?php
$this->breadcrumbs=array(
	'Gallerys'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-camera',
	'title'=>'Gallerys',
	'subtitle'=>'Data Gallerys',
);

$this->menu=array(
	array('label'=>'List Gallerys', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Gallerys', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Gallerys', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc, 'modelImage'=>$modelImage)); ?>
