<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}

	public function actionCreatecategory()
	{
		for ($i=1; $i < 111; $i++) { 
			$model = new PrdCategoryProduct;
			$model->category_id = 12;
			$model->product_id = $i;
			$model->save(false);
		}
	}

	public function actionInput()
	{
		$data = Table61::model()->findAll();
		foreach ($data as $key => $value) {
			$model = new PrdProduct;
			$model->kode = $value->col_1;
			if ($value->col_7 != '') {
				copy(YiiBase::getPathOfAlias('webroot').'/images/precise/'.$value->col_7.'/COVER.jpg', YiiBase::getPathOfAlias('webroot').'/images/product/'.Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-cover.jpg');
				$model->image = Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-cover.jpg';
			}else{
				$model->image = '';
			}
			$model->harga = $value->col_6;
			$model->harga_coret = 0;
			$model->save(false);
			$dataDesc = new PrdProductDescription;
			$dataDesc->product_id = $model->id;
			$dataDesc->language_id = 2;
			$dataDesc->name = $value->col_2;
			$dataDesc->subtitle = $value->col_3;
			$dataDesc->desc = '<p>'.$value->col_4.'</p>';
			$dataDesc->save(false);
			$dataAttr = explode(',', $value->col_5);
			foreach ($dataAttr as $v) {
				$modelAttr = new PrdProductAttributes;
				$modelAttr->product_id = $model->id;
				$modelAttr->attribute = trim($v);
				$modelAttr->stock = 10;
				$modelAttr->price = $value->col_6;
				$modelAttr->save(false);
				$modelAttr->id_str = $modelAttr->id;
				$modelAttr->save(false);

			}
			if ($value->col_7 != '') {
				for ($i=1; $i < 7; $i++) { 
					$modelImage = new PrdProductImage;
					$modelImage->product_id = $model->id;
					copy(YiiBase::getPathOfAlias('webroot').'/images/precise/'.$value->col_7.'/'.$i.'.jpg', YiiBase::getPathOfAlias('webroot').'/images/product/'.Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-photo'.$i.'.jpg');
					$modelImage->image = Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-photo'.$i.'.jpg';
					$modelImage->save(false);
				}
			}



		}
	}

	public function actionIndex()
	{
		// $criteria2=new CDbCriteria;
		// $criteria2->with = array('description');
		// $criteria2->order = 'date DESC';
		// $criteria2->addCondition('status = "1"');
		// $criteria2->addCondition('description.language_id = :language_id');
		// $criteria2->params[':language_id'] = $this->languageID;

		// if ($_GET['category']) {
		// 	$criteria = new CDbCriteria;
		// 	$criteria->with = array('description');
		// 	$criteria->addCondition('t.id = :id');
		// 	$criteria->params[':id'] = $_GET['category'];
		// 	$criteria->addCondition('t.type = :type');
		// 	$criteria->params[':type'] = 'category';
		// 	$criteria->order = 'sort ASC';
		// 	$strCategory = PrdCategory::model()->find($criteria);

		// 	// $inArray = PrdProduct::getInArrayCategory($_GET['category']);
		// 	// $criteria2->addInCondition('t.category_id', $inArray);
		// 	$criteria2->addCondition('t.tag LIKE :category');
		// 	$criteria2->params[':category'] = '%category='.$_GET['category'].',%';
		// }else{
		// 	$criteria2->addCondition('t.tag LIKE :category');
		// 	$criteria2->params[':category'] = '%category=35,%';
		// }
		// $pageSize = 8;

		// $product = new CActiveDataProvider('PrdProduct', array(
		// 	'criteria'=>$criteria2,
		//     'pagination'=>array(
		//         'pageSize'=>$pageSize,
		//     ),
		// ));

		// $model = new ContactForm;
		// $model->scenario = 'insert';

		$this->layout='//layouts/column1';

		$this->render('index', array(
			// 'product'=>$product,
			// 'model'=>$model,
		));
	}

	public function actionDining()
	{
		$this->layout='//layouts/column2';

		$this->pageTitle = 'Dining '.$this->pageTitle;
		$this->render('dining', array(
			'model'=>$model,
		));
	}


	public function actionError()
	{
		// $this->layout = '//layouts/error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{
				
				$this->layout='//layouts/column2';

				$this->pageTitle = 'Error '.$error['code'].': '. $error['message'] .' - '.$this->pageTitle;

				$this->render('error', array(
					'error'=>$error,
				));
			}
		}

	}

	public function actionAbout()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('about', array(	
		));
	}

	public function actionQuality()
	{
		$this->pageTitle = 'Quality - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('quality', array(	
		));
	}

	public function actionEvent()
	{
		$this->pageTitle = 'Events - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];

			if($model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
					'subject'=>'[UFO Elektronika] Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();
			}

		}

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('active = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->order = 'date_input DESC';
		$gallery = new CActiveDataProvider('Gallery', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>6,
		    ),
		));

		$this->render('event', array(
			'model'=>$model,
			'gallery'=>$gallery,
		));
	}

	public function actionEventdetail($id)
	{
		$this->pageTitle = 'Events - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('active = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $id;
		$criteria->order = 'date_input DESC';
		$detail = Gallery::model()->find($criteria);

		$this->render('eventdetail', array(
			'detail'=>$detail,
		));
	}

	public function actionBlogs()
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('active = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		if ($_GET['id']) {
			$criteria->addCondition('t.id = :id');
			$criteria->params[':id'] = $_GET['id'];
		}else{
			$criteria->order = 'date_input DESC';
		}
		if ($_GET['month']) {
			$criteria->addCondition('MONTH(date_input) = :month AND YEAR(date_input) = :year');
			$month = explode('-', $_GET['month']);
			$criteria->params[':month'] = $month[0];
			$criteria->params[':year'] = $month[1];
		}
		$dataBlog = Blog::model()->find($criteria);
		if($dataBlog===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$criteria = new CDbCriteria;
		$criteria->addCondition('active = "1"');
		$criteria->order = 'date_input DESC';
		$criteria->group = 'MONTH(date_input), YEAR(date_input)';
		$dataDate = Blog::model()->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->order = 'date_input DESC';
		$criteria->with = array('description');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('active = "1"');
		if ($_GET['month']) {
			$criteria->addCondition('MONTH(date_input) = :month AND YEAR(date_input) = :year');
			$month = explode('-', $_GET['month']);
			$criteria->params[':month'] = $month[0];
			$criteria->params[':year'] = $month[1];
		}else{
			$criteria->addCondition('t.id != :id');
			$criteria->params[':id'] = $dataBlog->id;
		}
		$dataBlogs = Blog::model()->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('active = "1"');
		$criteria->addCondition('date_input > :date_input');
		$criteria->params[':date_input'] = $dataBlog->date_input;
		$criteria->order = 'date_input ASC';
		$nextId = Blog::model()->find($criteria);

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('active = "1"');
		$criteria->addCondition('date_input < :date_input');
		$criteria->params[':date_input'] = $dataBlog->date_input;
		$criteria->order = 'date_input DESC';
		$prevId = Blog::model()->find($criteria);

		$this->pageTitle = $dataBlog->description->title . ' - Berita & Artikel - '.$this->pageTitle;
		// $this->pageTitle = 'Berita & Artikel - '.$this->pageTitle;
		$this->layout='//layouts/column3';

		$this->render('seen_on', array(
			'dataBlog'=>$dataBlog,
			'dataDate'=>$dataDate,
			'dataBlogs'=>$dataBlogs,
			'nextId'=>$nextId,
			'prevId'=>$prevId,
		));
	}

	public function actionProducts()
	{
		$model = new ContactForm;
		$model->scenario = 'insert';
		
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Products - '. $this->pageTitle;

		$data = DataProducts::nex_resource();

		$this->render('product', array(
			'product_resource'=>$data,
			'model'=>$model,
		));
	}

	public function actionNews()
	{
		$this->pageTitle = 'News & Article - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('news', array(	
		));
	}

	public function actionNewsDetail()
	{
		$this->pageTitle = 'News & Article - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('news_detail', array(	
		));
	}

	public function actionContact()
	{
		$this->layout='//layouts/column2';

		$this->pageTitle = 'Contact - '.$this->pageTitle;

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			$status = true;
	        $secret_key = "6LewK0EUAAAAAKBkAiXhXsR1ELPt7mQK5mcPRll2";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	          $status = false;
	          $model->addError('verifyCode', 'Verify you are not robbot');
	        }

			if($status AND $model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
					'subject'=>'['.Yii::app()->name.'] Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}

		$this->render('contact', array(
			'model'=>$model,
		));
	}

}


