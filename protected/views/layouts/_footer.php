
<section class="home-sec-3">
    <div class="row wow fadeInUp">
        <div class="col-md-30">
            <div class="box-content">
                <h3><?php echo $this->setting['home_section2_title'] ?></h3>
                <?php echo $this->setting['home_section2_content'] ?>
                <h4><a href="<?php echo $this->setting['home_section2_links'] ?>">CONTACT NIPPO</a></h4>
            </div>
        </div>
        <div class="col-md-30">
            <div class="image">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1057,585, '/images/static/'. $this->setting['home_section2_pictures'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img img-fluid">
            </div>
        </div>
    </div>
</section>


<section class="footer">
    <div class="prelative container">
        <div class="row wow fadeInDown">
            <div class="col-md-60">
                <div class="title">
                    <p>Get in touch with Nippo now</p>
                </div>
            </div>
            <div class="col-md-30">
                <div class="row">
                    <div class="col-md-30">
                        <div class="box-content">
                            <p>Email.<span> <a href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a></span></p>
                        </div>
                    </div>
                    <div class="col-md-30">
                        <div class="box-content">
                            <p>Whatsapp.<span> <a href="https://wa.me/<?php echo str_replace(' ', '', $this->setting['contact_wa']) ?>">+<?php echo $this->setting['contact_wa'] ?></a></span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-30">
                <div class="d-block d-sm-none py-3"></div>
                <div class="row">
                    <div class="col-md-20">
                        <h4>Nippo Gallery</h4>
                        <ul>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a></li>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>">Our Quality</a></li>
                            <li><a target="_blank" href="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['catalog_pdf'] ?>">Nippo Profile Book</a></li>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-md-20">
                        <?php 
                        $criteria = new CDbCriteria;
                        $criteria->addCondition('active = "1"');
                        $criteria->order = 't.date_input ASC';
                        $mod_kategh = ViewGallery::model()->findAll($criteria);
                        ?>
                        <h4>Gallery Colections</h4>
                        <ul>
                             <?php foreach ($mod_kategh as $key => $value): ?>
                              <li>
                                <a href="<?php echo CHtml::normalizeUrl(array('/home/dining', 'id'=>$value->id, 'names'=> Slug::Create($value->title))); ?>"><?php echo ucwords(strtolower($value->title)) ?></a>
                                </li>
                              <?php endforeach ?>
                        </ul>
                    </div>
                    <div class="col-md-20">
                        <h4>Social Media</h4>
                        <ul>
                            <li><a href="<?php echo $this->setting['url_instagram'] ?>">Instagram</a></li>
                            <li><a href="<?php echo $this->setting['url_youtube'] ?>">Youtube</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="footer-bawah">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-35">
                <div class="image">
                    <a target="_blank" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img class="img img-fluid" src="<?php echo $this->assetBaseurl; ?>Gallery-copy.png" alt=""></a>
                    <a target="_blank" href="https://www.nippotechs.com/"><img class="img img-fluid" src="<?php echo $this->assetBaseurl; ?>Techs.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-25">
                <div class="content">
                    <p>Copyright <?php echo date("Y"); ?> - Nippo Furniture Gallery Indonesia. All rights reserved.</p>
                    <p>
                        <a target="_blank" title="Website Design Surabaya" href="https://markdesign.net">Website design by Mark Design.</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="live-chat">
    <div class="row">
        <div class="col-md-60">
            <div class="live">
                <a href="https://wa.me/6281236111922">
                    <img src="<?php echo $this->assetBaseurl; ?>Whatsapp-Click-to-chat.png" class="img img-fluid" alt="">
                </a>
            </div>
        </div>
    </div>
</section>

<section class="nfoot_mob_wa d-block d-sm-none">
    <div class="row">
        <div class="col-md-60">
            <div class="nx_button">
                <a href="https://wa.me/6281236111922"><i class="fa fa-whatsapp"></i> Whatsapp</a>
            </div>
        </div>
    </div>
</section>