
<section class="home-sec-1">
    <div class="prelative container">
        <?php 
        $criteria = new CDbCriteria;
        $criteria->addCondition('active = "1"');
        $criteria->order = 't.date_input ASC';
        $mod_kategh = ViewGallery::model()->findAll($criteria);
        ?>
        <div class="row blocks_list_banner_kat">

            <?php foreach($mod_kategh as $key => $value): ?>
            <div class="col-md-15 col-30 wow fadeInDown">
                <div class="items">
                    <div class="image prelative">
                        <img class="w-100" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(403,465, '/images/gallery/'. $value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
                        <div class="inside">
                            <div class="n_tables">
                                <div class="n_middles">
                                    <h4><a href="<?php echo CHtml::normalizeUrl(array('/home/dining', 'id'=> $value->id, 'name'=>Slug::Create($value->title) )); ?>"><?php echo $value->title ?></a></h4>
                                    <p><a href="<?php echo CHtml::normalizeUrl(array('/home/dining', 'id'=> $value->id, 'name'=>Slug::Create($value->title) )); ?>">VIEW COLLECTION</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach ?>

        </div>
    </div>
</section>

<?php

$mod_icon= [
                [
                'file_image'=> $this->setting['home_section1_images_1'],
                'title'=> $this->setting['home_section1_titlesn_1'],
                ],
                [
                'file_image'=> $this->setting['home_section1_images_2'],
                'title'=> $this->setting['home_section1_titlesn_2'],
                ],
                [
                'file_image'=> $this->setting['home_section1_images_3'],
                'title'=> $this->setting['home_section1_titlesn_3'],
                ],
                ];

?>


<section class="home-sec-2">
    <div class="prelative container2">
        <div class="row">
            <div class="col-md-25">
                <div class="image wow fadeInDown"><img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(562,648, '/images/static/'. $this->setting['home_section1_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt=""></div>
            </div>
            <div class="col-md-35 wow fadeInDown">
                <div class="box-content">
                    <div class="content-inner">
                        <div class="title">
                            <h3><?php echo $this->setting['home_section1_titles'] ?></h3>
                        </div>
                        <div class="isi">
                            <?php echo $this->setting['home_section1_content'] ?>
                        </div>
                        <div class="value">
                            <p>NIPPO VALUE</p>
                        </div>
                        <div class="row">
                            <div class="col-md-45">
                                <div class="inners_list_icon row text-center">
                                    <?php foreach($mod_icon as $key => $value): ?>
                                    <div class="icon col">
                                        <img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $value['file_image'] ?>" class="img img-fluid" alt="">
                                        <p><?php echo $value['title'] ?></p>
                                    </div>
                                    <?php endforeach ?>
                                </div>
                                <div class="clear clearfix"></div>
                                
                            </div>
                            <div class="col-md-15"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(function(){

        $('.blocks_list_banner_kat .items .image').on('mouseenter', function(){
            
            $('.blocks_list_banner_kat .items .image .inside').hide();
            $(this).find('.inside').show();

            return false;
        })

    })
</script>