<?php 
$cover_page = '';

// if (isset($this->setting['about_hero_image'])) {
//   $cover_page = Yii::app()->baseUrl.ImageHelper::thumb(1920,562, '/images/static/'. $this->setting['about_hero_image'] , array('method' => 'adaptiveResize', 'quality' => '90'));
// }
// <section class="cover-insides" if ($cover_page): style="background-image: url(echo $cover_page);" endif>
?>

<div class="prelative container2">
  
  <section class="cover-insides prelatife">
    <div class="pictures_all wow fadeInDown">
      <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['about_hero_image']; ?>" alt="" class="img img-fluid">
    </div>
  	<div class="inners_cover wow fadeInDown">		
      <div class="inners_cvr">
  			<div class="row">
          <div class="col-md-60">
            <div class="texts text-center">
              <h1><?php echo $this->setting['about_hero_title'] ?></h1>
              <div class="py-2"></div>
              <p><?php echo $this->setting['about_hero_subtitle'] ?></p>
            </div>
          </div>
  			</div>
  		</div>

  	</div>
  </section>

</div>


<div class="prelative container2">
  <div class="py-4"></div>
  <div class="tops_desc content-text wow fadeInDown">
    <div class="row">
      <div class="col-md-15"></div>
      <div class="col-md-30">
        <div class="texts-inner text-center">
          <?php echo $this->setting['about1_content'] ?>
        </div>
      </div>
      <div class="col-md-15"></div>
    </div>
    <div class="clear clearfix"></div>
  </div>

    <div class="py-4"></div>

    <div class="row">
      <?php for ($i=1; $i < 4; $i++) { ?>
      <div class="col-md-20 col-20">
        <div class="bx_banner wow fadeInDown">
          <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['about1_pictures_t'. $i]; ?>" alt="" class="img img-fluid">
        </div>
      </div>
      <?php } ?>
    </div>

    <div class="py-3"></div>

    <div class="row">
      <div class="col-md-60 wow fadeInDown">
        <div class="bx_banner">
          <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['about1_pictures_tnn']; ?>" alt="" class="img img-fluid">
        </div>
      </div>
    </div>

    <div class="py-4 d-none d-sm-block"></div>
    <div class="py-1 d-block d-sm-none"></div>

    <div class="content-text py-4">
      <div class="row">
        <div class="col-md-15"></div>
        <div class="col-md-30 wow fadeInDown">
          <div class="texts-inner text-center">
            <?php echo $this->setting['about2_content_top'] ?>
          </div>
        </div>
        <div class="col-md-15"></div>
      </div>
    </div>

    <div class="py-4 d-none d-sm-block"></div>
    <div class="py-1 d-block d-sm-none"></div>
    
    <?php 
    $data_ngallery = [
                      [
                        'pictures'=> $this->assetBaseurl.'../../images/static/'. $this->setting['about2_pictures_t1'],
                        'titles'=> $this->setting['about2_title_t1'],
                        'text'=> $this->setting['about2_content_t1'],
                      ],
                      [
                        'pictures'=> $this->assetBaseurl.'../../images/static/'. $this->setting['about2_pictures_t2'],
                        'titles'=> $this->setting['about2_title_t2'],
                        'text'=> $this->setting['about2_content_t2'],
                      ],
                      [
                        'pictures'=> $this->assetBaseurl.'../../images/static/'. $this->setting['about2_pictures_t3'],
                        'titles'=> $this->setting['about2_title_t3'],
                        'text'=> $this->setting['about2_content_t3'],
                      ],
                      

                    ];
    ?>
    
    <div class="defaults_list_gallerys">

      <div class="d-none d-sm-block">
        <?php foreach ($data_ngallery as $key => $value): ?>
        <div class="lists_row wow fadeInDown">
          <div class="row no-gutters">
            <div class="col-md-20 <?php if (($key % 2) == 1): ?>order-sm-2<?php endif ?>">
              <div class="desc_info align-middle px-4">
                <div class="middles px-2">
                  <h4><?php echo $value['titles'] ?></h4>
                  <?php echo $value['text'] ?>
                </div>
              </div>
            </div>
            <div class="col-md-40 <?php if (($key % 2) == 1): ?>order-sm-1<?php endif ?>">
              <div class="bx_banner <?php if (($key % 2) == 1): ?>pr-4<?php else: ?>pl-4<?php endif ?>">
                <img src="<?php echo $value['pictures'] ?>" alt="" class="img img-fluid  <?php if (($key % 2) == 1): ?>pr-2<?php else: ?>pl-2<?php endif ?>">
              </div>
            </div>
          </div>
        </div>
        <div class="py-3"></div>
          <!-- end list -->
        <?php endforeach ?>
      </div>

      <div class="d-block d-sm-none">
        <?php foreach ($data_ngallery as $key => $value): ?>
        <div class="lists_row wow fadeInDown">
          <div class="row no-gutters">
            <div class="col-md-20 order-2">
              <div class="desc_info align-middle px-4">
                <div class="middles px-2">
                  <h4><?php echo $value['titles'] ?></h4>
                  <?php echo $value['text'] ?>
                </div>
              </div>
            </div>
            <div class="col-md-40 order-1">
              <div class="bx_banner">
                <img src="<?php echo $value['pictures'] ?>" alt="" class="img img-fluid">
              </div>
            </div>
          </div>
        </div>
        <div class="py-3"></div>
          <!-- end list -->
        <?php endforeach ?>
      </div>


      <div class="py-4"></div>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
</div>

    <div class="full_banner wow fadeInDown">
      <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['about3_pictures'] ?>" alt="" class="img img-fluid">
    </div>

    <div class="py-5 d-none d-sm-block"></div>
    <div class="py-3 d-block d-sm-none"></div>
    
    <div class="prelative container2">
      
      <div class="row topsn content-text wow fadeInUp">
        <div class="col-md-12"></div>
        <div class="col-md-36">
          <div class="text-center">
            <?php echo $this->setting['about3_content_btm'] ?>
          </div>
        </div>
        <div class="col-md-12"></div>
      </div>

      <div class="row topsn content-text wow fadeInUp">
        <div class="col-md-5"></div>
        <div class="col-md-50">
          
          <div class="py-1"></div>
          <div class="text-center">
            <ul class="list-inline text-center m-0">
              <li class="list-inline-item"><img src="<?php echo $this->assetBaseurl ?>log-brands_09.jpg" alt="" class="img img-fluid"></li>
              <li class="list-inline-item"><img src="<?php echo $this->assetBaseurl ?>log-brands_11.jpg" alt="" class="img img-fluid"></li>
              <li class="list-inline-item"><img src="<?php echo $this->assetBaseurl ?>log-brands_13.jpg" alt="" class="img img-fluid"></li>
              <li class="list-inline-item"><img src="<?php echo $this->assetBaseurl ?>log-brands_15.jpg" alt="" class="img img-fluid"></li>
            </ul>
          </div>
          
        </div>
        <div class="col-md-5"></div>
      </div>



      <div class="clear"></div>
    </div>

    <div class="py-5 d-none d-sm-block"></div>
    <div class="py-3 d-block d-sm-none"></div>
