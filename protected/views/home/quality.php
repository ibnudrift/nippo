<?php 
$cover_page = '';

// if (isset($this->setting['about_hero_image'])) {
//   $cover_page = Yii::app()->baseUrl.ImageHelper::thumb(1920,562, '/images/static/'. $this->setting['about_hero_image'] , array('method' => 'adaptiveResize', 'quality' => '90'));
// }
// <section class="cover-insides" if ($cover_page): style="background-image: url(echo $cover_page);" endif>
?>

<div class="prelative container2">
  
  <section class="cover-insides prelatife">
    <div class="pictures_all wow fadeInDown">
      <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['quality_hero_image']; ?>" alt="" class="img img-fluid">
    </div>
    <div class="inners_cover wow fadeInDown">    
      <div class="inners_cvr">
        <div class="row">
          <div class="col-md-60">
            <div class="texts text-center">
              <h1><?php echo $this->setting['quality_hero_title'] ?></h1>
              <div class="py-2"></div>
              <p><?php echo $this->setting['quality_hero_subtitle'] ?></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

</div>


<div class="prelative container2">
  <div class="py-4"></div>
  <div class="tops_desc content-text">
    <div class="row">
      <div class="col-md-15"></div>
      <div class="col-md-30 wow fadeInDown">
        <div class="texts-inner text-center">
         <?php echo $this->setting['quality1_content'] ?>
        </div>
      </div>
      <div class="col-md-15"></div>
    </div>
    <div class="clear clearfix"></div>
  </div>

    <div class="py-4"></div>
    
    <?php 
    $data_ngallery = [
                      [
                        'pictures'=> $this->assetBaseurl.'../../images/static/'. $this->setting['quality2_pictures_t1'],
                        'titles'=> $this->setting['quality2_title_t1'],
                        'text'=> $this->setting['quality2_content_t1'],
                      ],
                      [
                        'pictures'=> $this->assetBaseurl.'../../images/static/'. $this->setting['quality2_pictures_t2'],
                        'titles'=> $this->setting['quality2_title_t2'],
                        'text'=> $this->setting['quality2_content_t2'],
                      ],
                      [
                        'pictures'=> $this->assetBaseurl.'../../images/static/'. $this->setting['quality2_pictures_t3'],
                        'titles'=> $this->setting['quality2_title_t3'],
                        'text'=> $this->setting['quality2_content_t3'],
                      ],
                      [
                        'pictures'=> $this->assetBaseurl.'../../images/static/'. $this->setting['quality2_pictures_t4'],
                        'titles'=> $this->setting['quality2_title_t4'],
                        'text'=> $this->setting['quality2_content_t4'],
                      ],
                    ];
    ?>
    
    <div class="defaults_list_gallerys to_bottom qualitys">

      <?php foreach ($data_ngallery as $key => $value): ?>
      <div class="lists_row wow fadeInDown">
        <div class="row no-gutters">
          
          <div class="col-md-40 <?php if (($key % 2) == 1): ?>order-sm-2<?php endif ?>">
            <div class="bx_banner <?php if (($key % 2) == 1): ?>pl-1<?php else: ?>pr-3<?php endif ?>">
              <img src="<?php echo $value['pictures'] ?>" alt="" class="img img-fluid">
            </div>
          </div>

          <div class="col-md-20 <?php if (($key % 2) == 1): ?>order-sm-1<?php endif ?>">
            <div class="desc_info align-middle px-4">
              <div class="middles">
                <h4><?php echo $value['titles'] ?></h4>
                <p><?php echo $value['text'] ?></p>
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="py-4 d-none d-sm-block"></div>
      <div class="py-3 d-none d-sm-block"></div>

      <div class="py-3 d-block d-sm-none"></div>
        <!-- end list -->
      <?php endforeach ?>

      <div class="py-4"></div>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
</div>

    
    <div class="prelative container2">
      <div class="full_banner wow fadeInDown">
        <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['quality3_pictures'] ?>" alt="" class="img img-fluid">
      </div>
      <div class="py-4"></div>      
      <div class="row topsn content-text wow fadeInUp">
        <div class="col-md-12"></div>
        <div class="col-md-36">
          <div class="text-center">
            <?php echo $this->setting['quality3_content'] ?>
          </div>
        </div>
        <div class="col-md-12"></div>
      </div>



      <div class="clear"></div>
    </div>

    <div class="py-5 d-none d-sm-block"></div>
    <div class="py-3 d-block d-sm-none"></div>
