<?php 
$cover_page = '';

// if (isset($this->setting['about_hero_image'])) {
//   $cover_page = Yii::app()->baseUrl.ImageHelper::thumb(1920,562, '/images/static/'. $this->setting['about_hero_image'] , array('method' => 'adaptiveResize', 'quality' => '90'));
// }
// <section class="cover-insides" if ($cover_page): style="background-image: url(echo $cover_page);" endif>
?>

<div class="prelative container2">
  
  <section class="cover-insides prelatife">
    <div class="pictures_all">
      <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['contact_hero_image']; ?>" alt="" class="img img-fluid">
    </div>
    <div class="inners_cover">    
      <div class="inners_cvr">
        <div class="row">
          <div class="col-md-60">
            <div class="texts text-center">
              <h1><?php echo $this->setting['contact_hero_title'] ?></h1>
              <div class="py-2"></div>
              <p><?php echo $this->setting['contact_hero_subtitle'] ?></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

</div>


<div class="prelative container2">
  <div class="py-4"></div>
  <div class="tops_desc content-text">
    <div class="row">
      <div class="col-md-12"></div>
      <div class="col-md-36">
        <div class="texts-inner text-center">

          <?php echo $this->setting['contact_content_top'] ?>

          <div class="py-4"></div>
          <div class="desc_contact">
            <div class="row">
              <div class="col-md-30 text-left">
                <p><strong>Visit Nippo Gallery at this opening hours:</strong></p>
                <p>
                  <?php echo nl2br($this->setting['contact_content']) ?>
                </p>
                <div class="py-2"></div>
                <p><strong>Nippo Gallery Address & Contact Details</strong></p>
                <p>
                  <?php echo nl2br($this->setting['contact_content2']) ?>
                  <br><a target="_blank" href="<?php echo $this->setting['contact1_links_map'] ?>">Click here to view at Google Map</a><br>Tel. <?php echo $this->setting['contact_phone'] ?><br>Email: <a href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a></p>
                <div class="py-2"></div>
                <p><strong>Whatsapp:</strong><strong><br><a href="https://wa.me/<?php echo str_replace(' ', '', $this->setting['contact_wa']); ?>"><i class="fa fa-whatsapp"></i> &nbsp;+<?php echo $this->setting['contact_wa'] ?></a></strong></p>
              </div>
              <div class="col-md-30">
                <div class="full_banner"><img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['contact_feature_image']; ?>" alt="" class="img img-fluid"></div>
              </div>
            </div>
            <div class="clear"></div>
          </div>          
        </div>
      </div>
      <div class="col-md-12"></div>
    </div>
    <div class="clear clearfix"></div>
  </div>


    <div class="clear"></div>
</div>

<div class="py-4"></div>
<div class="py-2"></div>
